import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.*
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.*
import kotlin.system.exitProcess

/*
 * Compose Calculator
 * (c) 2024 Jeff Avery
 * Based on the earlier `calculator-javafx` project.
 */

var expr by mutableStateOf(Expr(0, Expr.OP.NONE, 0))
var display by mutableStateOf("")

fun main() = application {
    Window(
        title = "Compose Calculator",
        onCloseRequest = ::exitApplication,
        state = WindowState(
            placement = WindowPlacement.Floating,
            position = WindowPosition.Aligned(Alignment.Center),
            width = 240.dp, height = 282.dp),
        resizable = false,
        onKeyEvent = {
            if (it.type == KeyEventType.KeyUp) {
                if (it.isShiftPressed) {
                    handleShiftedAction(it.key)
                } else {
                    handleAction(it.key)
                }
            }
            false
        }
    ) {
        Calculator()
    }
}

class Expr(var num1: Int, var op: OP = OP.NONE, var num2: Int) {
    enum class OP { ADD, SUB, MUL, DIV, NONE }

    fun calculate(): Int {
        return when(op) {
            OP.ADD -> num1 + num2
            OP.SUB -> num1 - num2
            OP.MUL -> num1 * num2
            OP.DIV -> num1 / num2
            else -> num1
        }
    }
    fun clear() = run { num1 = 0 ; op = OP.NONE; num2 = 0 }
    fun set(operation: OP) = run { op = operation }
    fun set(n: Int) = run { if (op == OP.NONE) num1 = (num1 * 10) + n else num2 = (num2 * 10) + n }
}

fun handleShiftedAction(key: Key) {
    // for some odd reason, the + on my keyboard isn't picked up.
    // this will manually catch `SHIFT =` which is the same thing.
    when(key) {
        Key.Equals -> { expr.set(Expr.OP.ADD); display += "+" }
    }
}

fun handleAction(key: Key) {
    when(key) {
        Key.A -> { expr.clear(); display = ""}
        Key.C -> { expr.clear(); display = ""  }
        Key.D -> { }
        Key.One -> { expr.set(1); display += "1" }
        Key.Two -> { expr.set(2); display += "2" }
        Key.Three -> { expr.set(3); display += "3" }
        Key.Four -> { expr.set(4); display += "4" }
        Key.Five -> { expr.set(5); display += "5" }
        Key.Six -> { expr.set(6); display += "6" }
        Key.Seven -> { expr.set(7); display += "7" }
        Key.Eight -> { expr.set(8); display += "8" }
        Key.Nine -> { expr.set(9); display += "9" }
        Key.Zero -> { expr.set(0); display += "0" }
        Key.Multiply -> { expr.set(Expr.OP.MUL); display += "*" }
        Key.Slash -> { expr.set(Expr.OP.DIV); display += "/" }
        Key.Plus -> { expr.set(Expr.OP.ADD); display += "+" }
        Key.Minus -> { expr.set(Expr.OP.SUB); display += "-" }
        Key.Equals -> {
            expr.num1 = expr.calculate();
            display = display + "=" + expr.num1;
            expr.num2 = 0;
            expr.op = Expr.OP.NONE }
        Key.Q -> { exitProcess(0) }
    }
}

@Composable
@Preview
fun Calculator() {
    val modifier = Modifier.padding(1.dp).size(58.dp, 35.dp)
    MaterialTheme {
        Column {
            OutlinedTextField(
                TextFieldValue(display),
                onValueChange = {  },
                readOnly = true,
                modifier = Modifier.size(300.dp, 55.dp),
                textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.End))
            Row {
                Button(modifier = modifier, onClick = { handleAction(Key.A)} ) { Text("A") }
                Button(modifier = modifier, onClick = { handleAction(Key.C)} ) { Text("C") }
                Button(modifier = modifier, onClick = { handleAction(Key.Delete)} ) { Text("Del") }
                Button(modifier = modifier, onClick = { handleAction(Key.Multiply)} ) { Text("*") }

            }
            Row {
                Button(modifier = modifier, onClick = { handleAction(Key.Seven)} ) { Text("7") }
                Button(modifier = modifier, onClick = { handleAction(Key.Eight)} ) { Text("8") }
                Button(modifier = modifier, onClick = { handleAction(Key.Nine)} ) { Text("9") }
                Button(modifier = modifier, onClick = { handleAction(Key.Slash)} ) { Text("/") }
            }
            Row {
                Button(modifier = modifier, onClick = { handleAction(Key.Four)} ) { Text("4") }
                Button(modifier = modifier, onClick = { handleAction(Key.Five)} ) { Text("5") }
                Button(modifier = modifier, onClick = { handleAction(Key.Six)} ) { Text("6") }
                Button(modifier = modifier, onClick = { handleAction(Key.Plus)} ) { Text("+") }
            }
            Row {
                Button(modifier = modifier, onClick = { handleAction(Key.One)} ) { Text("1") }
                Button(modifier = modifier, onClick = { handleAction(Key.Two)} ) { Text("2") }
                Button(modifier = modifier, onClick = { handleAction(Key.Three)} ) { Text("3") }
                Button(modifier = modifier, onClick = { handleAction(Key.Minus)} ) { Text("-") }
            }
            Row {
                Button(modifier = modifier, onClick = { handleAction(Key.Period)} ) { Text(".") }
                Button(modifier = modifier, onClick = { handleAction(Key.Zero)} ) { Text("0") }
                Button(modifier = modifier, onClick = { handleAction(Key.NumLock)} ) { Text("+/-") }
                Button(modifier = modifier, onClick = { handleAction(Key.Equals)} ) { Text("=") }
            }
        }
    }
}


